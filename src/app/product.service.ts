import { Injectable } from '@angular/core';
import { Product } from './product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private products: Product[] = [
    {
      id: 'p1',
      merk: 'RAM 8gb',
      foto: ['https://via.placeholder.com/600/d32776',"https://via.placeholder.com/600/24f355"],
      model: 'V-GEN',
      jenis: 2,
      stock: 100,
      harga: '4000',
      speed: '100',
      ukuran: '8gb'
    },
    {
      id: 'p2',
      merk: 'RAM 4gb',
      foto: ['https://via.placeholder.com/600/f66b97','https://via.placeholder.com/600/d32776',],
      model: 'V-GEN',
      jenis: 2,
      stock: 100,
      harga: '4000',
      speed: '100',

      ukuran: '4gb'
      
    }
  ]
  constructor() { }

  getAllProducts(){
    return [...this.products]
  }
  getProduct(id: String){
    return {...this.products.find(product=>{
      return product.id === id;
    })};
  }
  addProduct(data: Product){
    this.products.push(data);
  }
  deleteProduct(productId: String){
    this.products = this.products.filter(product=>{
      return product.id !== productId;
    })
  }
  editProduct(data: Product){
    this.products = this.products.map((item)=>{
      if(item.id == data.id){
        return data
      }else{
        return item;
      }
    })
  }
}
