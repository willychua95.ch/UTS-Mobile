import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  products: Product[];
  constructor(private productsService: ProductService) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.products = this.productsService.getAllProducts().filter((item)=>{
      return item.stock > 0;
    })
  }

}
