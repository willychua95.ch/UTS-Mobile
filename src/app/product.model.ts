export interface Product {
    id: string;
    foto: string[];
    merk: string;
    model: string;
    harga: string;
    stock: Number;
    jenis: Number;
    base?: String;
    boost?: String;
    core?: String;
    thread?: String;
    speed?: String;
    ukuran?: String;
    chipset?: String;
    prosesor?: String;
}