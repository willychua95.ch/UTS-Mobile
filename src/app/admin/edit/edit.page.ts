import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/product.model';
import { ProductService } from 'src/app/product.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  loadedProduct: Product;
  nama: String;
  foto: String;
  idJenis: String;
  listFoto: string;
  type: any[] = [
    {
      id: 1,
      value: 'Prosessor'
    },
    {
      id: 2,
      value: 'RAM'
    },
    {
      id: 3,
      value: 'MotherBoard'
    },
    {
      id: 4,
      value: 'GPU'
    },

  ]

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router:Router

  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap=>{
      if(!paramMap.has('id')){ return; }
      this.loadedProduct = this.productService.getProduct(paramMap.get('id'))
      this.nama = this.loadedProduct.merk
      this.listFoto = this.loadedProduct.foto.join();
      this.idJenis = this.loadedProduct.jenis.toString();
    })
  }
  compareWithFn = (o1, o2) => {
    return o1 == o2;
  };

  compareWith = this.compareWithFn;
  onSubmit(form: NgForm){
    let data: Product;
    data = {
        id: this.loadedProduct.id,
        foto: form.value.foto.split(','), 
        jenis: this.loadedProduct.jenis, 
        merk: form.value.nama, 
        model: form.value.model, 
        harga: form.value.harga, 
        stock: form.value.stock,
    }
    if(this.idJenis == '1'){
      data =  {
        ...data,      
        base: form.value.base,
        boost: form.value.boost,
        core: form.value.core,
        thread: form.value.thread,
      }
    }else if(this.idJenis == '2'){
      data =  {
        ...data,
        speed: form.value.speed,
        ukuran: form.value.ukuran,
      }
    }
    else if(this.idJenis == '3'){
      data =  {
        ...data,
        chipset: form.value.chipset,
        prosesor: form.value.prosesor,
      }
    }
    this.productService.editProduct(data)
    this.router.navigate(['/admin'])
  }
}
