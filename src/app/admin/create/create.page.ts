import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Product } from 'src/app/product.model';
import { ProductService } from 'src/app/product.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
  idJenis: String;
  product: Product[];
  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit() {
    this.product = this.productService.getAllProducts();
  }

  onSubmit(form: NgForm){
    let data: Product;
    console.log(form);
    const jenis = form.value.jenis
    data = {
        id: `p${this.product.length + 1}`,
        foto: form.value.foto.split(','), 
        jenis: form.value.jenis, 
        merk: form.value.nama, 
        model: form.value.model, 
        harga: form.value.harga, 
        stock: form.value.stock,
    }
    if(jenis == '1'){
      data =  {
        ...data,      
        base: form.value.base,
        boost: form.value.boost,
        core: form.value.core,
        thread: form.value.thread,
      }
    }else if(jenis == '2'){
      data =  {
        ...data,
        speed: form.value.speed,
        ukuran: form.value.ukuran,
      }
    }
    else if(jenis == '3'){
      data =  {
        ...data,
        chipset: form.value.chipset,
        prosesor: form.value.prosesor,
      }
    }
    this.productService.addProduct(data);
    this.router.navigate(['/admin'])
    
  }
  onSend(){

  }
  handleChange(data: String){
    this.idJenis = data;
  }
}
