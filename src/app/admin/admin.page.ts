import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { Product } from '../product.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {
  products: Product[];
  constructor(
    private productsService: ProductService,
    private alertCtrl: AlertController,
    private toastController: ToastController,
    ) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.products = this.productsService.getAllProducts();

  }
  del(id: String){
    this.productsService.deleteProduct(id);
    this.products = this.productsService.getAllProducts();
    this.presentToast();

  }
  async presentAlert(id: String){
    const alert = await this.alertCtrl.create({
      header: 'Are you sure?',
      message: 'Do you really want to delete this contact?',
      buttons: [
        {text:'Cancel', role:'cancel'},
        {text:'Delete', handler: ()=>this.del(id)}
      ]
    });
    await alert.present();
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Berhasil di delete',
      duration: 2000
    });
    toast.present();
  }

}
